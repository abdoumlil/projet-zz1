#include "grid.h"
#include "engine.h"

/* ************** EVENTS ********************** */
/* Echap pour revenir à la page menu principal  */
/* double Echap pour quitter depuis le jeu      */
/* numbers to choose config                     */      


int main(int argc, char * argv[]) { 
    //SDL 
    SDL_Window* window = NULL;
    SDL_Renderer * renders;
    SDL_Texture * background = NULL;
    SDL_DisplayMode screen;

    /* ****************** SDL init ********************** */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renders);

    /* ****************** IMG init ********************** */
    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);
    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
        end_sdl(0, "ERROR SDL INIT", window, renders);
    }

    /* ******************* Création de la fenêtre ********************** */
    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);

    int window_width  = screen.w * 0.66;
    int window_height = screen.h * 0.66;
    window = SDL_CreateWindow("Premier dessin",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 
                            window_width,
                            window_height,
                            SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renders);


    /* ******************* Création du render ********************** */
    renders = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(renders == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renders);

    background = load_texture_from_image("backgrnd.png", window, renders);


    //SDL_Rect rectangle;                                             
    SDL_bool program_on = SDL_TRUE;   

    while (program_on){
        SDL_RenderClear(renders);
        draw_background(background, window, renders);
        SDL_RenderPresent(renders);                               
        SDL_Event event;       
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){                      
                case SDL_QUIT :                           
                    program_on = SDL_FALSE;                 
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        case SDLK_1: //Glider
                            load_config(1, window, renders);
                            break;
                        case SDLK_2: //Spaceship
                            load_config(2, window, renders);
                            break;
                        case SDLK_3: //Blinkers
                            load_config(3, window, renders);
                            break;
                        case SDLK_4: //Aleatoire
                            load_config(4, window, renders);
                            break;
                        default:
                            break;
                    }
                default:                                  
                    break;
            }
        };
        SDL_SetWindowSize(window, window_width, window_height);
    }

    IMG_Quit();
    end_sdl(1, "Normal ending", window, renders);
    return 0;
}
   


