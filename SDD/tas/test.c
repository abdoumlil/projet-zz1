#include "test.h"



void p_queue_test() {

    int error_code = 0;
    heap_data_t default_value = {INT_MIN};

    p_queue_t q = init_queue(20, &error_code);
    int i = 0;

    
    heap_data_t v;
    for(int i = 0; i < 5; ++i) {
        v.element = 1 + rand()%20;
        p_queue_push(&q, v);
    }
    
    heap_print(q.heap);

    while(!p_queue_is_empty(&q)) {
        printf("%d ", p_queue_top(&q, default_value, &error_code).element);
        p_queue_pop(&q, default_value, &error_code);
    }
    printf("\n");
    p_queue_release(&q);
}


void heap_test() {

    int error_code = 0;
    heap_data_t default_value = {INT_MIN};

    heap_t myheap = heap_create(20, &error_code);
    srand(time(NULL));

    char array[11][20] = {"17", "15", "13", "9", "6", "5", "10", "4", "8", "3", "1"};

    for(int i = 0; i < 11; ++i) {
        heap_data_t data;
        data.element = atoi(array[i]);
        myheap.tab[i] = data;
        myheap.last_position++;
    }

    heap_print(myheap);

    printf("building heap \n");
    build_min_heap(myheap);

    heap_print(myheap);
    printf("\n");

    //printf("prior element : %d\n ", heap_minimum(myheap)->element);
    
    heap_data_t min = heap_extract_min_or_default(&myheap, default_value, &error_code);
    printf("prior element : %d\n", min.element);
    heap_print(myheap);


    min = heap_extract_min_or_default(&myheap, default_value, &error_code);
    printf("prior element : %d\n", min.element);
    heap_print(myheap);

    
    min = heap_extract_min_or_default(&myheap, default_value, &error_code);
    printf("prior element : %d\n", min.element);
    heap_print(myheap);


    printf("adding -10 ... \n");
    heap_data_t nouv;
    nouv.element = -10;
    heap_add(&myheap, nouv);

    printf("adding 99 \n");
    nouv.element = 99;
    heap_add(&myheap, nouv);

    printf("adding 7 \n");
    nouv.element = 7;
    heap_add(&myheap, nouv);

    heap_print(myheap);

    heap_sort(&myheap);
    
    for(int i = 0; i < myheap.capacity; ++i) {
        printf("%d ", myheap.tab[i].element);
    }

    printf("\n");

    release_heap(&myheap);
}