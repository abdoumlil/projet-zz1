#ifndef P_QUEUE_H
#define P_QUEUE_H


#include "heap.h"
#include "utile.h"


typedef struct p_queue p_queue_t;


struct p_queue {
    heap_t heap;
};

p_queue_t     init_queue(int capacity, error_code_t * error_code);
heap_data_t   p_queue_top(p_queue_t  * q, heap_data_t default_value, error_code_t * error_code);
void          p_queue_pop(p_queue_t  * q, heap_data_t default_value, error_code_t * error_code);
void          p_queue_push(p_queue_t * q, heap_data_t data, error_code_t * er_code);
void          p_queue_release(p_queue_t * q);
int           p_queue_is_empty(p_queue_t * q);


#endif