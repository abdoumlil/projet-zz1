#ifndef SORTING_H
#define SORTING_H

#include "heap.h"


void heap_sort(heap_t * heap, error_code_t * error_code);
int cmpfunc (const void * a, const void * b);
void sorted_array_print(heap_t heap, FILE * stream);

#endif