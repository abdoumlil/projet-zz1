#include "graph_algos.h"
#include "labyrinth.h"

//dot -Tpng first_graph.gv -o file.png

int main() {
    error_code_t err = none;
    maze_t mymaze = generate_random_maze(4,4,&err,fisher_yate);
    draw_graph(mymaze.graph, "graph_imgs/couv.gv");

    int * path = NULL;
    int * parent = dijkstra(mymaze.graph, 0,7, &path, &err);

    printf("\n");
    for(int i = 0; i < mymaze.graph.nb_nodes; ++i) {
        printf("%d ", parent[i]);
    } 

    free(path);
    free(parent);
    release_maze(&mymaze,4);
    return 0;
}