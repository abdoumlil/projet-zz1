#ifndef UTILE_H
#define UTILE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>   
#include <limits.h>
typedef enum error_code error_code_t;

enum error_code {
    none,
    empty_heap,
    allocation_error,
    invalid_node,
    null_ptr_error
};

#endif