#ifndef GRAPH_ALGOS_H
#define GRAPH_ALGOS_H

#include "partition.h"
#include "graphe.h"
#include "matrix.h"
#include "p_queue.h"

typedef enum colors {
    WHITE,
    GREY,
    BLACK
}colors_t;

//function pointer
typedef void (*predicat_organizing_edges) (edge_t *, int);
typedef int (*predicat_distance) (int , int , int);

//acm
partition_t calcul_composantes_connexes(graphe_t g, error_code_t * err);
graphe_t    kruskal(graphe_t g, error_code_t * err, predicat_organizing_edges edge_org);
void        fisher_yate(edge_t *edges_list, int n);

//shortest path
int *       dijkstra(graphe_t g, int starting_node, int destination_node, int ** path, int * path_l, error_code_t * err);
int * A_star(graphe_t g, int starting_node, int destination_node, predicat_distance func,int columns, int ** path, int * path_l, error_code_t * err);

//depth first
int *       depth_first_search(graphe_t g, int starting_node);
void        DFS_rec(graphe_t g, int starting_node, colors_t * colors, int * parent);

//usefull functions for the algorithms above
int     cmp_edges(const void *e1, const void *e2);
edge_t  search_for_edge(graphe_t g, int x, int y, int * found);
void    initialisation(graphe_t g, int * dist, int * parent, int starting_node);

//distance calculation
int * length_path(int end, int start, int * parent, int * path_l);
int manhattan(int node1,int node2,int columns);
int tchebychev(int node1,int node2,int columns);
int eucli(int node1,int node2,int columns);


#endif