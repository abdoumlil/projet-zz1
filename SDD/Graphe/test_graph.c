#include "graph_algos.h"
#include "labyrinth.h"

//dot -Tpng first_graph.gv -o file.png

int main() {
    graphe_t g2;
    partition_t p;
    error_code_t err = none;
    int nb_edges_max = pow(12, 2) / 2;
    init_graphe(&g2, 5, nb_edges_max,&err);
    g2.nb_edges = 6;
    g2.edges_list[0]  = create_edge(0, 1, 20);
    g2.edges_list[1]  = create_edge(0, 2, 3);
    g2.edges_list[2]  = create_edge(1, 3, 4);
    g2.edges_list[3]  = create_edge(2, 3, 10);
    g2.edges_list[4]  = create_edge(2, 4, 2);
    g2.edges_list[5]  = create_edge(3, 4, 1);

    
    g2.edges_list[6]  = create_edge(2, 5, 2);
    
    g2.edges_list[7]  = create_edge(2, 6, 1);
    g2.edges_list[8]  = create_edge(1, 2, 6);
    g2.edges_list[9]  = create_edge(6, 7, 3);
    g2.edges_list[10]  = create_edge(1, 7, 2);

    draw_graph(g2, "graph_imgs/dij.gv");
    
    draw_graph(g2, "graph_imgs/first_graph.gv");
    p = calcul_composantes_connexes(g2, &err);
    partitions_listing(&p, stdout, &err);


    
    printf("KRUSKAL TEST \n");
    graphe_t abr_couv = kruskal(g2, &err,sortArrays);
    draw_graph(abr_couv, "graph_imgs/abr_couv.gv");

    
    printf("KRUSKAL TEST \n");
    graphe_t abr_couv2 = kruskal(g2, &err,fisher_yate);
    draw_graph(abr_couv2, "graph_imgs/abr_couv_2.gv");
    

    release_graph(&g2);
   //release_graph(&abr_couv2);
    release_partition(&p);
    
    //release_graph(&abr_couv); 
    return 0;
}