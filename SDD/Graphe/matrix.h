#ifndef MATRIX_H
#define MATRIX_H

#include "utile.h"


int   **init_matrix(int capacity, error_code_t *err);
void    release_matrix(int *** mat, int capacity);
int   **random_adjacency_matrix(int capacity, error_code_t *err);
void    print_matrice(int **mat, int capacity);



#endif