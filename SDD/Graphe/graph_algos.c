#include "graph_algos.h"
#include "labyrinth.h"

partition_t calcul_composantes_connexes(graphe_t g, error_code_t * err) {
    partition_t p;
    //representer_graphe(g);

    int liste[g.nb_nodes];
    for(int i=0; i<g.nb_nodes; i++) {
        liste[i] = i;
    }

    p = create_partition(liste, g.nb_nodes, err);
    if(*err == none) {
        for(int i = 0; i < g.nb_edges; i++) {

            int node_1 = g.edges_list[i].i;
            int node_2 = g.edges_list[i].j;
            fusion(&p, node_1, node_2, err);

        }
    }

    return p;
}

graphe_t kruskal(graphe_t g, error_code_t * err, predicat_organizing_edges edge_org) { 

    //Etape 1
    graphe_t abr_couv;
    edge_org(g.edges_list, g.nb_edges);

    //Etape 2
    int nodes[g.nb_nodes];
    for(int i = 0; i < g.nb_nodes; ++i) {
        nodes[i] = i;
    }

    partition_t p  = create_partition(nodes, g.nb_nodes, err);

    if(*err == none) {
        //Etape 3
        int nb_max_edges = pow(g.nb_nodes, 2) / 2;
        init_graphe(&abr_couv, g.nb_nodes, nb_max_edges, err);
        draw_graph(abr_couv, "graph_imgs/solo.gv");
        
        if(*err == none) {
            int j = 0;
            for(int i = 0; i < g.nb_edges; ++i) {
                int node_1 =  g.edges_list[i].i;
                int node_2 =  g.edges_list[i].j;
                int weight =  g.edges_list[i].weight;
                if(search_for_root(&p, node_1, err) != search_for_root(&p, node_2, err)) {
                    fusion(&p, node_1, node_2, err);
                    abr_couv.edges_list[j++] = create_edge(node_1, node_2, weight);
                    abr_couv.nb_edges++;
                }
            }
        }
        else {
            *err = allocation_error;
        }
    } else {
        *err = allocation_error;
    }
    
    release_partition(&p);
    return abr_couv;
}

int cmp_edges(const void *e1, const void *e2) {
	const edge_t *f1 = e1;
	const edge_t *f2 = e2;
	if (f1->weight == f2->weight)
		return 0;
	if (f1->weight < f2->weight)
		return -1;
	return 1;
}

void sortArrays(edge_t * arr, int nb_edges) {
    qsort(arr, nb_edges, sizeof(edge_t), cmp_edges);
}

void fisher_yate(edge_t *edges_list, int n) {
    srand(time(NULL));
    for(int i=0; i<n-2; i++) {
        int lower = i;
        int upper = n-1;
        int j = (rand() % (upper - lower + 1)) + lower;
        edge_t tmp = edges_list[i];
        edges_list[i] = edges_list[j];
        edges_list[j] = tmp;
    }
}

void initialisation(graphe_t g, int * dist, int * parent, int starting_node) {
    for(int i = 0; i < g.nb_nodes; ++i) {
        dist[i] = 99;
        parent[i] = -1;
    }
    dist[starting_node] = 0;
}

int * dijkstra(graphe_t g, int starting_node, int destination_node, int ** path, int * path_l, error_code_t * err) {

    int *parent = malloc(g.nb_nodes * sizeof(int));
    int dist[g.nb_nodes];

    initialisation(g, dist, parent, starting_node);
    p_queue_t Q = init_queue(g.nb_nodes, err);
    heap_data_t def_value = {-1, -1};
    int found = 0;

    for(int i = 0; i < g.nb_nodes; ++i) {
        heap_data_t data = {i, dist[i]};
        p_queue_push(&Q, data, err);
    }

    int stop = 0;
    while(!p_queue_is_empty(&Q) && !stop) {
        heap_data_t node1 = p_queue_top(&Q, def_value, err);
        p_queue_pop(&Q, def_value, err);
        if(node1.node == destination_node) {
            stop = 1;
        } else {
            for(int voisin = 0; voisin < g.nb_nodes; ++voisin) {
            found = 0;
            edge_t e = search_for_edge(g, node1.node, voisin, &found);
                if(found) {
                    if(dist[voisin] > dist[node1.node] + e.weight) {
                        dist[voisin]   = dist[node1.node] + e.weight;
                        parent[voisin] = node1.node;
                        remplacer_cle(&Q, voisin, dist[voisin]);
                    }
                }
            }
        }
    }

    //longueuer du chemin
    *path = length_path(destination_node, starting_node, parent, path_l);
    if(*path == NULL) {
        *err = allocation_error;
    }
    printf("\n");
    p_queue_release(&Q);
    return parent;
}

int * A_star(graphe_t g, int starting_node, int destination_node, predicat_distance func,int columns, int ** path, int * path_l, error_code_t * err) {

    int *parent = malloc(g.nb_nodes * sizeof(int));
    int dist[g.nb_nodes];

    //initialisation_A_star(g, dist, parent, starting_node, destination_node, func,columns);
    initialisation(g, dist, parent, starting_node);
    p_queue_t Q = init_queue(g.nb_nodes, err);
    heap_data_t def_value = {-1, -1};
    int found = 0;

    for(int i = 0; i < g.nb_nodes; ++i) {
        heap_data_t data = {i, dist[i]};
        p_queue_push(&Q, data, err);
    }

    int stop = 0;
    while(!p_queue_is_empty(&Q) && !stop) {
        heap_data_t node1 = p_queue_top(&Q, def_value, err);
        p_queue_pop(&Q, def_value, err);
        if(node1.node == destination_node) {
            stop = 1;
        } else {
            for(int voisin = 0; voisin < g.nb_nodes; ++voisin) {
            found = 0;
            edge_t e = search_for_edge(g, node1.node, voisin, &found);
                if(found) {
                    if(dist[voisin] > dist[node1.node] + e.weight) {
                        dist[voisin]   = dist[node1.node] + e.weight + func(voisin, destination_node, columns);
                        parent[voisin] = node1.node;
                        remplacer_cle(&Q, voisin, dist[voisin]);
                    }
                }
            }
        }
    }

    //longueuer du chemin
    *path = length_path(destination_node, starting_node, parent, path_l);
    if(*path == NULL) {
        *err = allocation_error;
    }
    p_queue_release(&Q);
    return parent;
}

int * depth_first_search(graphe_t g, int starting_node) {

    colors_t array_colors[g.nb_nodes];
    int * parent = malloc(g.nb_nodes*sizeof(int));
    
    //initialisation
    for(int i = 0; i < g.nb_nodes; ++i) {
        parent[i] = -1;
        array_colors[i] = WHITE;
    }

    DFS_rec(g, starting_node, array_colors, parent);
    return parent;
}

void DFS_rec(graphe_t g, int starting_node, colors_t * colors, int * parent) {

    colors[starting_node] = GREY;
    int found = 0;
    for(int voisin = 0; voisin < g.nb_nodes; ++voisin) {
        found = 0;
        search_for_edge(g, starting_node, voisin, &found);
        if(found) {
            if(colors[voisin] == WHITE) {
                parent[voisin] = starting_node;
                DFS_rec(g, voisin, colors, parent);
            }
        }
    }
    colors[starting_node] = BLACK;
}


int * length_path(int end, int start, int * parent, int * path_l) {
    
    *path_l = 0;
    int debut  = start;
    int destination    = end;
    while(parent[destination] != debut && parent[destination] != -1) {
        destination    = parent[destination];
        *path_l = *path_l + 1;
    }
    if(parent[destination] != -1) {
        *path_l = *path_l + 2;
    }
    printf("---> LONGUEUR Chemin: %d\n", *path_l);

    int i      = *path_l - 1;
    int * path = NULL;
    destination = end;

    if(*path_l > 0) {

        path = malloc(*path_l * sizeof(int));

        if(path != NULL) {

            for(int i = 0; i < *path_l; ++i) {
                path[i] = -1;
            }

            while(i > -1 && destination != -1) {
                path[i--] = destination;
                destination = parent[destination];
            }
            printf("chemin parcouru\n");
            for(int i = 0; i < *path_l; ++i) {
                printf("%d ---> ", path[i]);
            }

        } else {
            path = NULL;
        }
    }

    return path;
}

edge_t search_for_edge(graphe_t g, int x, int y, int * found) {
    int working = 1;
    int i=0;
    *found = 0;
    edge_t result = {-1, -1, -1} ;
    while(working && i < g.nb_edges) {
        if((g.edges_list[i].i == x && g.edges_list[i].j == y) || (g.edges_list[i].i == y && g.edges_list[i].j == x)) {
            working = 0;
            result.i = g.edges_list[i].i;
            result.j = g.edges_list[i].j;
            result.weight = g.edges_list[i].weight;
            *found = 1;
        }
        i++;
    }
    return result;
}

int manhattan(int node1,int node2,int columns)
{
    int n1_r = get_row_index(node1,columns);
    int n2_r = get_row_index(node2,columns);
    int n1_c = get_column_index(node1,columns);
    int n2_c = get_column_index(node2,columns);
    int distance = abs(n1_r - n2_r) + abs(n1_c - n2_c);
    return distance;
}

int tchebychev(int node1,int node2,int columns)
{
    int distance;
    int n1_r = get_row_index(node1,columns);
    int n2_r = get_row_index(node2,columns);
    int n1_c = get_column_index(node1,columns);
    int n2_c = get_column_index(node2,columns);
    int distance1 = abs(n1_r - n2_r); 
    int distance2 = abs(n1_c - n2_c);
    if (distance1 < distance2) distance = distance2;
    else distance = distance1;
    return distance;
}

int eucli(int node1,int node2,int columns)
{
    int n1_r = get_row_index(node1,columns);
    int n2_r = get_row_index(node2,columns);
    int n1_c = get_column_index(node1,columns);
    int n2_c = get_column_index(node2,columns);
    int distance = sqrt(pow((n1_r - n2_r),2) + pow((n1_c - n2_c),2));
    return distance;
}