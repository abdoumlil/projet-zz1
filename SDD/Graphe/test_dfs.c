#include "graph_algos.h"
#include "labyrinth.h"



int main() {
     
    graphe_t g2;
    error_code_t err = none;
    int nb_edges_max = pow(7, 2) / 2;
    init_graphe(&g2, 8, nb_edges_max,&err);
    g2.nb_edges = 10;
    g2.edges_list[0]  = create_edge(2, 4, 20);
    g2.edges_list[1]  = create_edge(1, 2, 3);
    g2.edges_list[2]  = create_edge(1, 6, 4);
    g2.edges_list[3]  = create_edge(5, 6, 10);
    g2.edges_list[4]  = create_edge(1, 5, 2);
    g2.edges_list[5]  = create_edge(5, 7, 1);
    g2.edges_list[6]  = create_edge(3, 5, 1);
    g2.edges_list[7]  = create_edge(0, 3, 1);
    g2.edges_list[8]  = create_edge(0, 7, 1);
    g2.edges_list[9]  = create_edge(2, 7, 1);


    draw_graph(g2, "graph_imgs/dfs.gv");

    int * parent = depth_first_search(g2, 2);
    for(int i = 0; i < g2.nb_nodes; ++i) {
        printf("%d ", parent[i]);
    } 

    release_graph(&g2);
    free(parent);
    return 0;
}