#include "partition.h"




void init_partition(partition_t * p, int capacity, error_code_t *err) {
    p->capacity = 0;
    p->height_array = NULL;
    p->parent = NULL;
    if(capacity > 0){
        p->parent    = malloc(capacity * sizeof(p->parent));
        p->height_array = malloc(capacity * sizeof(p->height_array));
        p->capacity = capacity;   
    }

    if(p->parent == NULL || p->height_array == NULL || capacity < 0) {
        *err = allocation_error;
    }
}


partition_t create_partition(int * array, int capacity, error_code_t *err) {
    partition_t p;
    init_partition(&p, capacity, err);
    if(*err != allocation_error) {
        for(int i = 0; i < capacity; ++i) {
            p.height_array[i] = 1;
            p.parent[i] = array[i];
        }
    }
    return p;
}

int search_for_root(partition_t *p, int node, error_code_t * err) {
    int root = -1;
    if(node > p->capacity || node < 0) {
        *err = invalid_node;
    }
    else {
        int current = node;
        while(p->parent[current] != current) {
            current = p->parent[current];
        }
        root = current;
    }

    return root;
}

int look_for_class(partition_t *p, int node, error_code_t * err) {
    return search_for_root(p, node, err);
}

void fusion(partition_t *p, int first_node, int second_node, error_code_t * err) {
    
    if(*err == none) {
        int root_of_first_node  = search_for_root(p, first_node, err);
        int root_of_second_node = search_for_root(p, second_node, err);

        if(*err != invalid_node) {
            if(p->height_array[root_of_first_node] > p->height_array[root_of_second_node]) {
                p->parent[root_of_second_node] = root_of_first_node;
            }
            else if (p->height_array[root_of_first_node] < p->height_array[root_of_second_node]) {
                p->parent[root_of_first_node] = root_of_second_node;
            }
            else {
                p->parent[root_of_first_node] = root_of_second_node;
                p->height_array[root_of_second_node]++;           
            }
        }
    }
}


void class_listing(partition_t * p, int classe, FILE * stream, error_code_t *err) {

    int root = -1;
    fprintf(stream, "{");
    for(int i = 0; i < p->capacity; ++i) {
        root = search_for_root(p, i, err);
        if(root == classe) {
            fprintf(stream, " %d ", i);
        }
    }
    fprintf(stream, "}");
}


void partitions_listing(partition_t *p, FILE *stream, error_code_t * err) {
    fprintf(stream, "{");
    for(int i=0; i<(p->capacity); i++) {
        if(p->parent[i] == i) {
            class_listing(p, i, stream, err);
            fprintf(stream, ",");  
        } 
    }
    fprintf(stream, "}\n");
}


void release_partition(partition_t * p) {
    free(p->height_array);
    free(p->parent);
    p->height_array = NULL;
    p->parent = NULL;
}
