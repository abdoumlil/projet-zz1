#ifndef VECT_H
#define VECT_H


typedef struct vector vect2D_t;


struct vector {
    double x;
    double y;
};

vect2D_t add(vect2D_t v1, vect2D_t v2);
vect2D_t init_vect(double x, double y);

#endif
