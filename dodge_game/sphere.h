#ifndef SPHERE_H
#define SPHERE_H


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "vect.h"

typedef struct boule boule_t;

struct boule {
    SDL_Rect destination;
    vect2D_t position;
    vect2D_t velocity;
    vect2D_t gravity;
};

SDL_Rect init_destination(double w, double h, int x, int y);
boule_t create_boule(vect2D_t pos, vect2D_t vel, vect2D_t grav, SDL_Rect destination);
void update_bool_coordinates(boule_t * boules, int number_of_boules, SDL_Rect windows_dimensions);

#endif