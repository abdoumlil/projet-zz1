#include "game_engine.h"
#include <time.h>


int main() {
    SDL_Window * window = NULL;
    SDL_Texture * ball = NULL, 
                * background = NULL, 
                * menu_bg = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_DisplayMode screen;
    srand(time(NULL));
    /*********************************************************************************************************************/  
    /*                         Initialisation de la SDL  + gestion de l'échec possible                                   */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);


    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);
    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    }

    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);

    /* Création de la fenêtre */
    window = SDL_CreateWindow("dodge ball",
                                SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, screen.w * 0.66,
                                screen.h * 0.66,
                                SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);

    /* Création du renderer */
    renderer  = SDL_CreateRenderer(
            window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) end_sdl(0, "ERROR RENDERER CREATION", window, renderer);

    ball = load_texture_from_image("sphere.png", window, renderer);
    background = load_texture_from_image("backgrnd.jpg", window, renderer);
    menu_bg = load_texture_from_image("space_to_play.png", window, renderer);

    //SDL_Rect rectangle;                                             
    SDL_bool program_on = SDL_TRUE;   

    while (program_on){
        SDL_RenderClear(renderer);
        draw_background(menu_bg, window, renderer);
        SDL_RenderPresent(renderer);                               
        SDL_Event event;       
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){                      
                case SDL_QUIT :                           
                    program_on = SDL_FALSE;                 
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        case SDLK_SPACE:
                            game(background, ball, window, renderer, 50);
                            break;
                        default:
                            break;
                    }
                default:                                  
                    break;
            }
        }
    }

    IMG_Quit();
    SDL_DestroyTexture(ball);
    SDL_DestroyTexture(background);
    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}