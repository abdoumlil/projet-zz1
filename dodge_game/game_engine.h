#ifndef game_engine_H
#define game_engine_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>




SDL_Texture* load_texture_from_image(char  *  file_image_name, 
                                            SDL_Window *window, 
                                            SDL_Renderer *renderer );

void game(SDL_Texture *bg_texture,
                                    SDL_Texture * balls_textures,
                                    SDL_Window* window,
                                    SDL_Renderer* renderer, int number_of_balls);

void draw_background(SDL_Texture *my_texture, 
                                SDL_Window *window, 
                                SDL_Renderer *renderer);

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer);

int load_img_library();



#endif