#include "game_engine.h"
#include "vect.h"
#include "sphere.h"
#include "player.h"

SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}


void draw_background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer) {

  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                   // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &destination);                 // Création de l'élément à afficher
}

void game(SDL_Texture *bg_texture,
                                    SDL_Texture * my_texture,
                                    SDL_Window* window,
                                    SDL_Renderer* renderer, int number_of_balls) {
    SDL_Rect 
          source = {0}, 
          source_player = {0},                       
          window_dimensions = {0},              
          destination = {0}, state = {0}, destination_player = {0};
    
    double zoom = 0.05;
    
    //dimension window
    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);

    //chargement des textures de mouvement et initialisation du joueur
    SDL_Texture * player_moves[3];
    player_moves[LEFT]    = load_texture_from_image("moves/LEFT.png", window, renderer);
    player_moves[RIGHT]   = load_texture_from_image("moves/RIGHT.png", window, renderer);
    player_moves[NEUTRAL] = load_texture_from_image("moves/NEUTRAL.png", window, renderer);

    SDL_QueryTexture(player_moves[LEFT], NULL, NULL, &source_player.w, &source_player.h);
    
    vect2D_t player_position = init_vect(0, window_dimensions.h - 280);

    destination_player.x = player_position.x;
    destination_player.y = player_position.y;
    destination_player.h = source_player.h * 3;
    destination_player.w = source_player.w * 2.5;

    player_t player = create_player(player_position, destination_player);


    

    //initialisation et gestion des dimensions des boules
    SDL_QueryTexture(my_texture, NULL, NULL, &source.w, &source.h);
    int offset_x = source.w, 
        offset_y = source.h;
    
    boule_t * boules = malloc(number_of_balls * sizeof(boule_t));

    for(int i = 0; i < number_of_balls; ++i) {
        double x = rand()%window_dimensions.w + 50;
        double y = (rand()%(window_dimensions.h/2) + 1);
        vect2D_t pos  = init_vect(x, y);
        vect2D_t vel  =  init_vect(2, 3);
        vect2D_t grav = init_vect(0, 0.6);
        SDL_Rect destinationpri = init_destination(offset_x * zoom, offset_y * zoom, pos.x, pos.y);
        boules[i] = create_boule(pos, vel, grav, destinationpri);
    }


    //boucle du jeu
    SDL_bool program_on = SDL_TRUE;
    p_orientation_n p_direction = NEUTRAL;
    int initial_number_of_balls = 3;

    while (program_on){                       
        SDL_Event event;  
        SDL_RenderClear(renderer);
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){                      
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        case SDLK_LEFT: //move left
                            p_direction = LEFT;
                            break;
                        case SDLK_RIGHT: //move right
                            p_direction = RIGHT;
                            break;
                        case SDLK_UP:
                            p_direction = NEUTRAL;
                            break;
                        case SDLK_a:
                            initial_number_of_balls++;
                            if(initial_number_of_balls > number_of_balls) {
                                initial_number_of_balls = number_of_balls;
                            }
                            break;
                        default:
                            break;
                }
                default:                                  
                    break;
            }
        }

        update_bool_coordinates(boules, initial_number_of_balls, window_dimensions);
        draw_background(bg_texture, window, renderer);
        
        player = update_player_coordinates(player, p_direction, window_dimensions);


        for(int j = 0; j < initial_number_of_balls; ++j) {
            SDL_RenderCopy(renderer, my_texture,
                                &source,
                                &boules[j].destination);
            //test colllision
            if( (boules[j].destination.x >= player.destination.x &&  boules[j].destination.x <= player.destination.x + player.destination.w) 
                &&   (boules[j].destination.y >= player.destination.y && boules[j].destination.y <= player.destination.y + player.destination.h)) {
                program_on = SDL_FALSE;
                break;    
            }
        }
        
        SDL_RenderCopy(renderer, player_moves[p_direction], &source_player, &player.destination);
        SDL_RenderPresent(renderer);         
        SDL_Delay(10);               
    }

    SDL_RenderClear(renderer);
    free(boules);
}


void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                


  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}