    graphe_t g2;
    partition_t p;
    error_code_t err = none;
    int nb_edges_max = pow(12, 2) / 2;
    init_graphe(&g2, 12, nb_edges_max,&err);
    g2.nb_edges = 12;
    g2.edges_list[0]  = create_edge(0, 1, 2);
    g2.edges_list[1]  = create_edge(0, 2, 4);
    g2.edges_list[2]  = create_edge(3, 1, 5);
    g2.edges_list[3]  = create_edge(1, 2, 3);
    g2.edges_list[4]  = create_edge(4, 5, 1);
    g2.edges_list[5]  = create_edge(7, 10, -2);
    g2.edges_list[6]  = create_edge(7, 8, 10);
    g2.edges_list[7]  = create_edge(7, 9, 78);
    g2.edges_list[8]  = create_edge(10, 9, -4);
    g2.edges_list[9]  = create_edge(8, 9, 9);
    g2.edges_list[10] = create_edge(8, 11, 3);
    g2.edges_list[11] = create_edge(9, 11, 2);

    draw_graph(g2, "graph_imgs/first_graph.gv");
    p = calcul_composantes_connexes(g2, &err);
    partitions_listing(&p, stdout, &err);


    printf("KRUSKAL TEST \n");
    graphe_t abr_couv = kruskal(g2, &err,sortArrays);
    draw_graph(abr_couv, "graph_imgs/abr_couv.gv");

    printf("KRUSKAL TEST \n");
    graphe_t abr_couv2 = kruskal(g2, &err,fisher_yate);
    draw_graph(abr_couv2, "graph_imgs/abr_couv_2.gv");

    generate_random_maze(4, 4, &err);

    release_graph(&abr_couv2);
    release_partition(&p);
    release_graph(&g2);
    release_graph(&abr_couv);




    /*
    graphe_t g2;
    partition_t p;
    error_code_t err = none;
    int nb_edges_max = pow(12, 2) / 2;
    init_graphe(&g2, 8, nb_edges_max,&err);
    g2.nb_edges = 11;
    g2.edges_list[0]  = create_edge(0, 4, 5);
    g2.edges_list[1]  = create_edge(4, 5, 1);
    g2.edges_list[2]  = create_edge(3, 5, 1);
    g2.edges_list[3]  = create_edge(0, 3, 2);


    g2.edges_list[4]  = create_edge(0, 2, 5);
    g2.edges_list[5]  = create_edge(2, 3, 4);
    g2.edges_list[6]  = create_edge(2, 5, 2);
    
    g2.edges_list[7]  = create_edge(2, 6, 1);
    g2.edges_list[8]  = create_edge(1, 2, 6);
    g2.edges_list[9]  = create_edge(6, 7, 3);
    g2.edges_list[10]  = create_edge(1, 7, 2);

    draw_graph(g2, "graph_imgs/dij.gv");
    /*
    draw_graph(g2, "graph_imgs/first_graph.gv");
    p = calcul_composantes_connexes(g2, &err);
    partitions_listing(&p, stdout, &err);


    printf("KRUSKAL TEST \n");
    graphe_t abr_couv = kruskal(g2, &err,sortArrays);
    draw_graph(abr_couv, "graph_imgs/abr_couv.gv");

    
    printf("KRUSKAL TEST \n");
    graphe_t abr_couv2 = kruskal(g2, &err,fisher_yate);
    draw_graph(abr_couv2, "graph_imgs/abr_couv_2.gv");*/

    /* int * parent = dijkstra(g2, 0);
    for(int i = 0; i < g2.nb_nodes; ++i) {
        printf("%d ", parent[i]);
    } */

   // release_graph(&abr_couv2);
    //release_partition(&p);
    //release_graph(&g2);
    //release_graph(&abr_couv); 




    maze_t generate_random_maze(int n, int p, error_code_t * err) {

    //initialisation du labyrinthe
    maze_t mymaze = allocate_maze(n, p, err);
    init_maze(&mymaze, n, p, err);

    //generation d'un graphe et calcul de l'arbre couvrant
    int nb_edges_max = pow(n*p, 2) / 2;
    graphe_t maze_graph = generate_random_graph(n*p, nb_edges_max, err);
    graphe_t abr_couv = kruskal(maze_graph, err, fisher_yate);
    draw_graph(abr_couv, "graph_imgs/abr_couv.gv");
    
    //remplissage du labyrinth
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < p; ++j) {
            int nord = (i-1)*p + j;
            int sud = (i+1)*p + j;
            int est = i*p + (j+1);
            int ouest = i*p + (j-1);
            int pos = i*p + j;
            if ( est_valide(nord, n*p) && does_graphe_contain_edge(abr_couv, pos, nord)) //haut
            {
                mymaze.mat[i][j] -= 1;
            }
            if ( est_valide(sud, n*p) && does_graphe_contain_edge(abr_couv, pos, sud)) //bas
            {
                mymaze.mat[i][j] -= 2;
            }
            if (est_valide(ouest, n*p) && does_graphe_contain_edge(abr_couv, pos, ouest)) //gauche
            {
                mymaze.mat[i][j] -= 4;
            }
            if (est_valide(est, n*p) && does_graphe_contain_edge(abr_couv, pos, est)) //droite
            {
                mymaze.mat[i][j] -= 8;
            }
        }
    }
    
    release_graph(&abr_couv);
    release_graph(&maze_graph);
    return mymaze;
}