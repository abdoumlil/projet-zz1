void show_maze(SDL_Window * window, SDL_Renderer * renderer, int n , int p) {

    error_code_t err = none;
    maze_t mymaze = generate_random_maze(n, p, &err, sortArrays);
    SDL_Rect  
          source_player = {0},                       
          window_dimensions = {0},            
          destination_player = {0},
          source_goal = {0},
          goal_dest = {0};
    
    double zoom = 0.03;
    int deb = 0;
    int fin = n*p / 2;

    int width  = 30;
    int height = 30;

    //SDL 
    int window_w = width * p;
    int window_h = height * n;
    
    if(err != none) {
        end_sdl(0, "erreur chargement labyrinthe", window, renderer);
    }

    SDL_Texture * player_moves = NULL;
    player_moves    = load_texture_from_image("textures/NEUTRAL.png", window, renderer);

    SDL_Texture * goal = NULL;
    goal    = load_texture_from_image("textures/sphere.png", window, renderer);

    SDL_QueryTexture(player_moves, NULL, NULL, &source_player.w, &source_player.h);
    SDL_QueryTexture(goal, NULL, NULL, &source_goal.w, &source_goal.h);

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    
    destination_player.x = 0;
    destination_player.y = 0;
    destination_player.h = source_player.h * 0.5;
    destination_player.w = source_player.w * 0.5;

    goal_dest.x = get_column_index(fin, p) * width;
    goal_dest.y = get_row_index(fin, p) * height;
    goal_dest.w = source_goal.w * zoom;
    goal_dest.h = source_goal.h * zoom;


    print_maze(&mymaze, n, p, &err);

    //SDL_RenderSetLogicalSize(renders, window_w, window_h);
    SDL_SetWindowSize(window, window_w, window_h);
    //SDL_RenderSetLogicalSize(renderer, window_w, window_h);
    //SDL_RenderSetScale( renderer, scale, scale);
    SDL_RenderPresent(renderer);
    

    int * path = NULL;
    int i = 0;
    int node = -1;
    
    int * parent = A_star(mymaze.graph, deb, fin, manhattan, p, &path, &err);
    


    SDL_bool program_on = SDL_TRUE;
    while (program_on && node != fin){    
        SDL_RenderClear(renderer);                   
        SDL_Event event;                        
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){  
                //touche clavier                    
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        default:
                            break;
                }
                default:                                  
                    break;
            }
        }
        int r = 255;
        int g = 255;
        int b = 255;
        int a = 0;
        SDL_SetRenderDrawColor(renderer, r, g, b, a);
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < p; ++j) {
                int walls = mymaze.mat[i][j];
                int x = j * width; 
                int y = i * height;
                if(walls & NORD) {
                    SDL_RenderDrawLine(renderer, x,       y,        x+width, y);
                }
                if(walls & SUD) {
                    SDL_RenderDrawLine(renderer, x,       y+height, x+width, y+height);
                }
                if(walls & OUEST) {
                    SDL_RenderDrawLine(renderer, x,       y,        x,       y+height);
                }
                if(walls & EST) {
                    SDL_RenderDrawLine(renderer, x+width, y,        x+width, y+height);
                }
            }
        }

        node = path[i++];
        printf("indice %d | noeud %d \n", i, node);
        int c = get_column_index(node, p);
        int l = get_row_index(node, p);

        int x = c * width; 
        int y = l * height;

        destination_player.x = x;
        destination_player.y = y;

        SDL_RenderCopy(renderer, player_moves, &source_player, &destination_player);
        SDL_RenderCopy(renderer, goal, &source_goal, &goal_dest);
        SDL_Delay(800);
        SDL_RenderPresent(renderer);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, a);
    }

    free(parent);
    free(path);
    release_maze(&mymaze, p);
}