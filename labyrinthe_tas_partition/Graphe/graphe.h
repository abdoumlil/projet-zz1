#ifndef GRAPHE_H
#define GRAPHE_H

#include "matrix.h"

typedef struct edge {
    int i;
    int j;
    int weight;
} edge_t;

typedef struct graphe_t {
    edge_t *edges_list;
    int     nb_nodes;
    int     nb_edges;
} graphe_t;


edge_t      create_edge(int x, int y, int w);
void        init_graphe(graphe_t *g, int n, int nb_edges, error_code_t *err);

graphe_t    generate_random_graph(int capacity, int nb_edges_max, error_code_t *err);

void        draw_graph(graphe_t g, const char * filename);
void        draw_graph_from_adj_matrix(int **mat,  int capcaity, const char * filename);

void        sortArrays(edge_t * arr, int nb_edges);
void        release_graph(graphe_t * graph);

#endif