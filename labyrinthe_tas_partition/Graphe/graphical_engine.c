#include "graphical_engine.h"

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                


  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
} 

void draw_background(SDL_Texture *my_texture, SDL_Window *window, SDL_Renderer *renderer) {

  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                   // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, my_texture,
                 &source,
                 &destination);                 // Création de l'élément à afficher
}

SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer ){
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}

void show_maze(SDL_Window * window, SDL_Renderer * renderer, int n , int p, int parcour_choice) {
    error_code_t err = none;
    int width  = 30;
    int height = 30;

    int r = 255, g = 255, b = 255, a = 0;
    //SDL 
    int window_w = width * p;
    int window_h = height * n;

    maze_t mymaze = generate_random_maze(n, p, &err, fisher_yate);
    print_maze(&mymaze, n, p, &err);

    //SDL_RenderSetLogicalSize(renders, window_w, window_h);
    SDL_SetWindowSize(window, window_w, window_h);
    //SDL_RenderSetLogicalSize(renderer, window_w, window_h);
    //SDL_RenderSetScale( renderer, scale, scale);
    SDL_RenderPresent(renderer);
    
    SDL_bool program_on = SDL_TRUE;

    int node_x[2] = {0, 0};
    int node_y[2] = {0, 0};
    int start_end[2] = {0, 0};
    int clicks = 0;

    SDL_Texture * maze_bg = load_texture_from_image("textures/background.png", window, renderer);
    if(maze_bg == NULL) end_sdl(0, "ERROR LOADING MAZE BG TEXTURE", window, renderer);

    while (program_on){    
        SDL_RenderClear(renderer);       
        draw_background(maze_bg, window, renderer);            
        SDL_Event event;                       
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){  
                //touche clavier                    
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        default:
                            break;
                }
                case SDL_MOUSEBUTTONDOWN:
                    if(SDL_GetMouseState(&node_x[clicks], &node_y[clicks]) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
                        int i = node_y[clicks] / height;
                        int j = node_x[clicks] / width;
                        start_end[clicks] = i * p + j;
                        printf("----------> node %d (>) %d\n", clicks, start_end[clicks]);
                        clicks++;
                        if(clicks > 1) {
                            clicks = 0;
                            maze_runner_dij_a_star(maze_bg, window, renderer, mymaze, n, p, start_end[0], start_end[1], parcour_choice);
                        }
                    }
                default:                                  
                    break;
            }
        }
        SDL_SetRenderDrawColor(renderer, r, g, b, a);
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < p; ++j) {
                int walls = mymaze.mat[i][j];
                int x = j * width; 
                int y = i * height;
                if(walls & NORD) {
                    SDL_RenderDrawLine(renderer, x,       y,        x+width, y);
                }
                if(walls & SUD) {
                    SDL_RenderDrawLine(renderer, x,       y+height, x+width, y+height);
                }
                if(walls & OUEST) {
                    SDL_RenderDrawLine(renderer, x,       y,        x,       y+height);
                }
                if(walls & EST) {
                    SDL_RenderDrawLine(renderer, x+width, y,        x+width, y+height);
                }
            }
        }

        SDL_RenderPresent(renderer);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, a);
    }

    release_maze(&mymaze, p);
}

void maze_runner_dij_a_star(SDL_Texture * bg, SDL_Window * window, SDL_Renderer * renderer, maze_t mymaze, int n , int p, int from, int to, int parcour_choice) {

    error_code_t err = none;
    
    //maze_t mymaze = generate_random_maze(n, p, &err, sortArrays);
    SDL_Rect  
          source_player = {0},                       
          window_dimensions = {0},            
          destination_player = {0},
          source_goal = {0},
          goal_dest = {0};
    
    double zoom = 0.03;
    int deb = from;
    int fin = to;

    int width  = 30;
    int height = 30;
    
    if(err != none) {
        end_sdl(0, "erreur chargement labyrinthe", window, renderer);
    }

    SDL_Texture * player_moves = NULL;
    player_moves    = load_texture_from_image("textures/NEUTRAL.png", window, renderer);

    SDL_Texture * goal = NULL;
    goal    = load_texture_from_image("textures/sphere.png", window, renderer);
    

    SDL_QueryTexture(player_moves, NULL, NULL, &source_player.w, &source_player.h);
    SDL_QueryTexture(goal, NULL, NULL, &source_goal.w, &source_goal.h);

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    
    destination_player.x = 0;
    destination_player.y = 0;
    destination_player.h = source_player.h * 0.5;
    destination_player.w = source_player.w * 0.5;

    goal_dest.x = get_column_index(fin, p) * width;
    goal_dest.y = get_row_index(fin, p) * height;
    goal_dest.w = source_goal.w * zoom;
    goal_dest.h = source_goal.h * zoom;

    int * path = NULL;
    int i = 0;
    int node = -1;
    int * parent = NULL;
    int path_l = 0;

    if(parcour_choice == 2) {
        parent = A_star(mymaze.graph, deb, fin, eucli, p, &path, &path_l, &err);
    } else if(parcour_choice == 1) {
        parent = dijkstra(mymaze.graph, deb, fin, &path, &path_l, &err);    
    }

    if(path_l != 0) {
        SDL_bool program_on = SDL_TRUE;
        while (program_on && node != fin){ 
            draw_background(bg, window, renderer);                     
            SDL_Event event;                        
            while(program_on && SDL_PollEvent(&event)){                                                                      
                switch(event.type){  
                    //touche clavier                    
                    case SDL_KEYDOWN:
                        switch(event.key.keysym.sym){ 
                            case SDLK_ESCAPE:
                                program_on = SDL_FALSE;
                                break;
                            default:
                                break;
                    }
                    default:                                  
                        break;
                }
            }
            int r = 255;
            int g = 255;
            int b = 255;
            int a = 0;
            SDL_SetRenderDrawColor(renderer, r, g, b, a);
            for(int i = 0; i < n; ++i) {
                for(int j = 0; j < p; ++j) {
                    int walls = mymaze.mat[i][j];
                    int x = j * width; 
                    int y = i * height;
                    if(walls & NORD) {
                        SDL_RenderDrawLine(renderer, x,       y,        x+width, y);
                    }
                    if(walls & SUD) {
                        SDL_RenderDrawLine(renderer, x,       y+height, x+width, y+height);
                    }
                    if(walls & OUEST) {
                        SDL_RenderDrawLine(renderer, x,       y,        x,       y+height);
                    }
                    if(walls & EST) {
                        SDL_RenderDrawLine(renderer, x+width, y,        x+width, y+height);
                    }
                }
            }

            node = path[i++];
            int c = get_column_index(node, p);
            int l = get_row_index(node, p);

            int x = c * width; 
            int y = l * height;

            destination_player.x = x;
            destination_player.y = y;

            SDL_RenderCopy(renderer, player_moves, &source_player, &destination_player);
            SDL_RenderCopy(renderer, goal, &source_goal, &goal_dest);
            SDL_Delay(100);
            SDL_RenderPresent(renderer);
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, a);
            SDL_RenderClear(renderer); 
        }
    } else {
        printf("pas de chemin possible entre le point de depart et d'arrive choisis\n");
    }

    free(parent);
    free(path);
}



void dfs(SDL_Window * window, SDL_Renderer * renderer, int n , int p) {


    error_code_t err = none;
    int width  = 30;
    int height = 30;
    //SDL 
    int window_w = width * p;
    int window_h = height * n;

    SDL_Rect  
          source_player = {0},                       
          window_dimensions = {0},            
          destination_player = {0};
        
    SDL_SetWindowSize(window, window_w, window_h);
    SDL_RenderPresent(renderer);

    if(err != none) {
        end_sdl(0, "erreur chargement labyrinthe", window, renderer);
    }

    SDL_Texture * player_moves = NULL;
    player_moves    = load_texture_from_image("textures/NEUTRAL.png", window, renderer);

    SDL_Texture * maze_bg = load_texture_from_image("textures/background.png", window, renderer);
    if(maze_bg == NULL) end_sdl(0, "ERROR LOADING MAZE BG TEXTURE", window, renderer);

    SDL_QueryTexture(player_moves, NULL, NULL, &source_player.w, &source_player.h);

    SDL_GetWindowSize(window, &window_dimensions.w, &window_dimensions.h);
    
    destination_player.x = 0;
    destination_player.y = 0;
    destination_player.h = source_player.h * 0.5;
    destination_player.w = source_player.w * 0.5;

    maze_t mymaze = generate_random_maze(n, p, &err, fisher_yate);
    print_maze(&mymaze, n, p, &err);

    /* initialisation tableau parent ET couleur */
    colors_t array_colors[mymaze.graph.nb_nodes];
    int * parent = malloc(mymaze.graph.nb_nodes*sizeof(int));
    for(int i = 0; i < mymaze.graph.nb_nodes; ++i) {
        parent[i] = -1;
        array_colors[i] = WHITE;
    }

    DFS_rec_graphic(renderer, window, maze_bg, player_moves, source_player, destination_player,
                    mymaze, 2, array_colors, parent, n, p);

    release_maze(&mymaze, p);
}


void DFS_rec_graphic(SDL_Renderer * renderer, SDL_Window * window, SDL_Texture * bg, SDL_Texture * player, SDL_Rect source_player, SDL_Rect destination_player,
                     maze_t mymaze, int starting_node, colors_t * colors, int * parent, int n, int p) {
    int c = 0, l = 0, x = 0, y = 0;
    int width  = 30;
    int height = 30;
    int r = 255, g = 255, b = 255, a = 0;


    //SDL_RenderClear(renderer);
    draw_background(bg, window, renderer);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < p; ++j) {
            int walls = mymaze.mat[i][j];
            int x = j * width; 
            int y = i * height;
            if(walls & NORD) {
                SDL_RenderDrawLine(renderer, x,       y,        x+width, y);
            }
            if(walls & SUD) {
                SDL_RenderDrawLine(renderer, x,       y+height, x+width, y+height);
            }
            if(walls & OUEST) {
                SDL_RenderDrawLine(renderer, x,       y,        x,       y+height);
            }
            if(walls & EST) {
                SDL_RenderDrawLine(renderer, x+width, y,        x+width, y+height);
            }
        }
    }

    c = get_column_index(starting_node, p);
    l = get_row_index(starting_node, p);

    x = c * width; 
    y = l * height;

    destination_player.x = x;
    destination_player.y = y;

    SDL_RenderCopy(renderer, player, &source_player, &destination_player);
    SDL_RenderPresent(renderer);
    SDL_Delay(100);

    colors[starting_node] = GREY;
    int found = 0;
    for(int voisin = 0; voisin < mymaze.graph.nb_nodes; ++voisin) {
        found = 0;
        search_for_edge(mymaze.graph, starting_node, voisin, &found);
        if(found) {
            if(colors[voisin] == WHITE) {
                parent[voisin] = starting_node;
                DFS_rec_graphic(renderer, window, bg, player,
                                source_player, destination_player,
                                mymaze, voisin, colors, parent, n, p);

                //SDL_RenderClear(renderer); 

                draw_background(bg, window, renderer);
                SDL_SetRenderDrawColor(renderer, r, g, b, a);
                for(int i = 0; i < n; ++i) {
                    for(int j = 0; j < p; ++j) {
                        int walls = mymaze.mat[i][j];
                        int x = j * width; 
                        int y = i * height;
                        if(walls & NORD) {
                            SDL_RenderDrawLine(renderer, x,       y,        x+width, y);
                        }
                        if(walls & SUD) {
                            SDL_RenderDrawLine(renderer, x,       y+height, x+width, y+height);
                        }
                        if(walls & OUEST) {
                            SDL_RenderDrawLine(renderer, x,       y,        x,       y+height);
                        }
                        if(walls & EST) {
                            SDL_RenderDrawLine(renderer, x+width, y,        x+width, y+height);
                        }
                    }
                }

                c = get_column_index(parent[voisin], p);
                l = get_row_index(parent[voisin], p);

                x = c * width; 
                y = l * height;

                destination_player.x = x;
                destination_player.y = y;

                SDL_RenderCopy(renderer, player, &source_player, &destination_player);
                SDL_RenderPresent(renderer);
                SDL_Delay(100);
            }
        }
        
    }
    colors[starting_node] = BLACK;
}
