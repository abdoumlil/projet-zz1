#include "graphical_engine.h"

int main(int argc, char * argv[]) { 
    (void) argc;
    //SDL 
    SDL_Window* window = NULL;
    SDL_Renderer * renderer = NULL;
    SDL_Texture * background = NULL;
    SDL_DisplayMode screen;

    /* ****************** SDL init ********************** */
    if (SDL_Init(SDL_INIT_VIDEO) != 0) end_sdl(0, "ERROR SDL INIT", window, renderer);

    /* ****************** IMG init ********************** */
    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);
    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
        end_sdl(0, "ERROR SDL INIT", window, renderer);
    }

    /* ******************* Création de la fenêtre ********************** */
    SDL_GetCurrentDisplayMode(0, &screen);
    printf("Résolution écran\n\tw : %d\n\th : %d\n", screen.w, screen.h);

    int window_width  = screen.w * 0.5;
    int window_height = screen.h * 0.5;
    window = SDL_CreateWindow("Premier dessin",
                            SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, 
                            window_width,
                            window_height,
                            SDL_WINDOW_OPENGL);
    if (window == NULL) end_sdl(0, "ERROR WINDOW CREATION", window, renderer);


    /* ******************* Création du render ********************** */
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(renderer == NULL) end_sdl(0, "ERROR RENDER LOAD", window, renderer);

    background = load_texture_from_image("textures/home_2.png", window, renderer);
    if(background == NULL) end_sdl(0, "ERROR BACKGROUND LOAD", window, renderer);

    //SDL_Rect rectangle;                                             
    SDL_bool program_on = SDL_TRUE;   
    int n = atoi(argv[1]), p = atoi(argv[2]);

    while (program_on){
        SDL_RenderClear(renderer);
        draw_background(background, window, renderer);
        SDL_RenderPresent(renderer);                               
        SDL_Event event;       
        while(program_on && SDL_PollEvent(&event)){                                                                      
            switch(event.type){                      
                case SDL_QUIT :                           
                    program_on = SDL_FALSE;                 
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym){ 
                        case SDLK_ESCAPE:
                            program_on = SDL_FALSE;
                            break;
                        case SDLK_1:
                            show_maze(window, renderer, n, p, 1);
                            break;
                        case SDLK_2:
                            show_maze(window, renderer, n, p, 1);
                            break;
                        case SDLK_3:
                            printf("c'est moi dfs en cours d'implementation! \n");
                            dfs(window, renderer, n , p);
                            break;
                        default:
                            break;
                    }
                default:                                  
                    break;
            }
        };
        SDL_SetWindowSize(window, window_width, window_height);
        SDL_Delay(500);
    }

    
    IMG_Quit();
    end_sdl(1, "Normal ending", window, renderer);
    return 0;
}
   