#include "heap.h"


heap_t heap_create(int capacity, error_code_t * error_code) {

    heap_t myheap = {0, 0, NULL};
    if(capacity > -1) {
        myheap.tab = (heap_data_t *) malloc(capacity * sizeof(heap_data_t));
    }

    if(myheap.tab == NULL) {
        *error_code = allocation_error;
    } else {
        for(int i = 0; i < capacity; ++i) {
            myheap.tab[i].element = 0;
        }
    }

    myheap.last_position = -1; //empty heap
    myheap.capacity = capacity;

    return myheap;
}

/* heap.tab != NULL*/
void build_min_heap(heap_t heap, error_code_t * error_code) {
    if(heap.tab == NULL) {
        *error_code = null_ptr_error;
    } else if(is_heap_empty(&heap)) {
        *error_code = empty_heap;
    } else {
        int middle = (heap.last_position) / 2;
        for(int i = middle ; i > -1; --i) {
            min_heapify_down(heap, i);
        }
    }
}

/* suppose that node is a valid index */
void min_heapify_down(heap_t heap, int node) {
    int left = left_child(node);
    int right = right_child(node);
    int min_index;
    if(left <= heap.last_position && heap.tab[left].element < heap.tab[node].element) {
        min_index = left;
    }
    else {
        min_index = node;
    }
    if(right <= heap.last_position && heap.tab[right].element <  heap.tab[min_index].element) {
        min_index = right;
    }
    if(min_index != node) {
        swap(heap, node, min_index);
        min_heapify_down(heap, min_index);
    }
}

/* suppose that node is a valid index */
void min_heapify_up(heap_t heap, int node) {
    if(node == 0) {
        return;
    }

    int parent = get_parent(node);

    if(heap.tab[parent].element > heap.tab[node].element) {
        swap(heap, parent, node);
        min_heapify_up(heap, parent);
    }
}

/* suppose that we have enough space in our array*/
void heap_add(heap_t * heap, heap_data_t data, error_code_t * error_code) {
    
    //to test array not full;
    if(heap != NULL && heap->tab != NULL) {
        if(is_heap_full(heap)) {
            heap_expand_size(heap, error_code);
        }
        if(*error_code != allocation_error) {
            heap->last_position++;
            heap->tab[heap->last_position] = data;
            min_heapify_up(*heap, heap->last_position);
        }
    }
    else {
        *error_code = null_ptr_error;
    }
}

/* error handling empty array  */
/* how to handle this to ask*/
heap_data_t heap_extract_min_or_default(heap_t * heap, heap_data_t default_value, error_code_t * error_code) {
    
    if(heap->tab == NULL) {
        *error_code = null_ptr_error;

    } else if (heap->last_position < 0) {
        *error_code = empty_heap;
    }
    
    if(*error_code != none) {
        return default_value;
    }
    
    heap_data_t min = heap->tab[0];
    swap(*heap, 0, heap->last_position);
    heap->last_position--;
    min_heapify_down(*heap, 0);

    return min;
}

/* error handling empty array  */ // --> OK
heap_data_t heap_minimum_or_default(heap_t heap, heap_data_t default_value, error_code_t * error_code) {
    
    heap_data_t min;
    if(heap.tab == NULL) {
        *error_code = null_ptr_error;
    } else if(heap.last_position < 0) {
        *error_code = empty_heap;
    }

    if(*error_code != none) {
        min = default_value;
    }
    else {
        min = heap.tab[0];
    }

    return min;
}

/* to handle index exceptions */
int left_child(int node) {
    return 2*node + 1;
}

int right_child(int node) {
    return 2*node + 2;
}

int get_parent(int node) {
    return (node - 1) / 2;
}

/* heap existance should be tested but in this case the swap function is called only on verified non NULL arrays */
void swap(heap_t heap, int from, int to) {
    heap_data_t tmp;
    tmp = heap.tab[from];
    heap.tab[from] = heap.tab[to];
    heap.tab[to] = tmp;
}

int is_heap_empty(heap_t * heap) {
    return heap->last_position <= -1;
}

int is_heap_full(heap_t * heap) {
    return heap->last_position == heap->capacity - 1;
}

void heap_print(heap_t heap, FILE * stream) {

    if(heap.tab != NULL && heap.last_position != -1) {
        for(int i = 0; i <= heap.last_position; ++i) {
            fprintf(stream, "%d ", heap.tab[i].element);
        }
    }
    else {
        printf("empty heap \n");
    }
    printf("\n");
}

void release_heap(heap_t * heap) {
    free(heap->tab);
    heap->tab = NULL;
}

void heap_expand_size(heap_t * heap, error_code_t * error_code) {
    int tmp = heap->capacity * 2;
    heap->tab = realloc(heap->tab, sizeof(heap_data_t) * tmp);
    if(heap->tab == NULL) {
        *error_code = allocation_error;
    }else {
        heap->capacity = tmp;
    }
}
